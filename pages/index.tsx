import type { NextPage } from 'next';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Link from 'library/components/material-ui/Link';
import { RootState } from 'store/registerReducers';
import { useSelector, useDispatch } from 'react-redux';
import ACTIONS from 'store/registerActions';

const Home: NextPage = () => {
  const nomr = useSelector<RootState, number>((datum) => datum.appState.count);

  const dispatch = useDispatch();

  const onIncrement = (): void => {
    dispatch(ACTIONS.Increment());
  };
  return (
    <Container maxWidth="lg">
      <Box
        sx={{
          my: 5,
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Typography component="h1" color="primary">
          Material UI v5 with Next.js in TypeScript
        </Typography>
        <Typography component="h2" color="secondary">
          Boilerplate for <Link href="/blog">building faster.</Link>
        </Typography>
        <button onClick={onIncrement}>+1</button>
        <div>{nomr}</div>
      </Box>
    </Container>
  );
};

export default Home;
