# Boiler Plate

- Nextjs ✅
- Storybook ✅
- Eslint ✅
- Material-ui ✅
- Redux-saga

## Installation

Install my-project with yarn

```bash
yarn
```

## Usage/Examples Material-Ui

```javascript
import Link from 'library/components/material-ui/Link';

function App() {
  return <Link />;
}
```

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`API_KEY`

`ANOTHER_API_KEY`

## Feedback

If you have any feedback, please reach out to us at nanda.rana@realco.co.id
