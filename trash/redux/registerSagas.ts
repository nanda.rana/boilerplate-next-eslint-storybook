import { all } from 'redux-saga/effects';

function* rootSaga() {
  yield all([
    // ...otherSaga
  ]);
}

export default rootSaga;
