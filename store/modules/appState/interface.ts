import { actionTypes } from './constants';

export interface AppState {
  count: number;
  error: null | Error;
}

export type Action = Increment | Decrement | Reset;

export interface Increment {
  type: actionTypes.INCREMENT;
}

export interface Decrement {
  type: actionTypes.DECREMENT;
}

export interface Reset {
  type: actionTypes.RESET;
}
