import { actionTypes } from './constants';
import * as actionIs from './interface';

export function Increment(): actionIs.Increment {
  return { type: actionTypes.INCREMENT };
}
