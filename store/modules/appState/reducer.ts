import { AppState, Action } from './interface';
import { actionTypes } from './constants';
import { HYDRATE } from 'next-redux-wrapper';

export const InitialState: AppState = {
  count: 0,
  error: null,
};

const reducer = (
  state = InitialState,
  action: Action | { type: typeof HYDRATE; payload: AppState }
): AppState => {
  switch (action.type) {
    case HYDRATE:
      return { ...state, ...action.payload };

    case actionTypes.INCREMENT:
      return {
        ...state,
        ...{ count: state.count + 1 },
      };

    case actionTypes.DECREMENT:
      return {
        ...state,
        ...{ count: state.count - 1 },
      };

    case actionTypes.RESET:
      return {
        ...state,
        ...{ count: InitialState.count },
      };

    default:
      return state;
  }
};

export default reducer;
