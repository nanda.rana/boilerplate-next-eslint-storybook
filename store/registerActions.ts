import * as appStateActions from './modules/appState/actions';

export default {
  ...appStateActions,
};
