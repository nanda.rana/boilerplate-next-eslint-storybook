import * as AppStateInterfaces from './modules/appState/interface';

export default {
  ...AppStateInterfaces,
};
