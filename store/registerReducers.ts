import { combineReducers } from 'redux';
import appState from './modules/appState/reducer';
import { AppState } from './modules/appState/interface';

export type RootState = {
  appState: AppState;
};

export default combineReducers({
  appState,
});
